#include "parser.h"


bool Parser::parse()
{
  pos = 0u;
  return KONF() && pos == tokensList.size();
}

bool Parser::match(int tokenType)
{
  return pos < tokensList.size() && tokenType == tokensList.at(pos++).getTokenType(); 
}

bool Parser::KONF()
{
  std::size_t savedPos = pos;
  return  (RED() && match(TOKEN_NEWLINE) && KONF()) ||
          (pos = savedPos, epsilon());
}

bool Parser::RED()
{
  std::size_t savedPos = pos;
  return  (match(TOKEN_ID) && match(TOKEN_EQUALS) && VRIJEDNOST()) ||
          (pos = savedPos, match(TOKEN_SECTION));
}

bool Parser::VRIJEDNOST()
{
  std::size_t savedPos = pos;
  return  match(TOKEN_NUM) ||
          (pos = savedPos, match(TOKEN_STRING));
}

bool Parser::epsilon()
{
  return true;
}

