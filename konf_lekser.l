
%{
#include "token.h"
%}

delim               [ \t]
ws                  {delim}+
id                  [a-zA-Z_][a-zA-Z_0-9]*
num                 [0-9]+
string              \"[^\"]*\"
section             \#[^\n]+
newline             \n
equals              \=

%%

{ws}
{id}               { return TOKEN_ID; }
{num}              { return TOKEN_NUM; }
{string}           { return TOKEN_STRING; }
{section}          { return TOKEN_SECTION; }
{newline}          { return TOKEN_NEWLINE; }
{equals}           { return TOKEN_EQUALS; }
.                  { return TOKEN_INVALID; }

%%
