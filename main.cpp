#include <iostream>
#include <vector>

#include "token.h"
#include "lex.yy.h"
#include "parser.h"

int main(void)
{
  using namespace std;
  vector<Token> tokens;
  int token;

  while( (token = yylex()) )
    tokens.push_back(Token(token, yytext));

  
  /*
  for(const auto& elem : tokens)
    switch(elem.getTokenType())
    {
      case TOKEN_NUM:
        std::cout << "<NUM>";
        break;

      case TOKEN_STRING:
        std::cout << "<STRING>";
        break;

      case TOKEN_ID:
        std::cout << "<ID>";
        break;

      case TOKEN_SECTION:
        std::cout << "<SECTION>";
        break;

      case TOKEN_NEWLINE:
        std::cout << "<NEWLINE>";
        break;

      case TOKEN_EQUALS:
        std::cout << "<EQUALS>";
        break;

      case TOKEN_INVALID:
        std::cout << "<INVALID>";
        break;
    }
   */
 
  Parser p(tokens);
  if(p.parse())
    cout << "Ispravna sintanska" << endl;
  else
    cout << "Neispravna sintaksa" << endl;
  return 0; 
}
