#ifndef TOKEN_H
#define TOKEN_H

#include <string>


#define TOKEN_NUM 0x1
#define TOKEN_STRING 0x2
#define TOKEN_ID 0x3
#define TOKEN_SECTION 0x4
#define TOKEN_NEWLINE 0x5
#define TOKEN_EQUALS 0x6
#define TOKEN_INVALID 0x7


class Token{
  public:
    Token(int tt, const std::string& l):
      tokenType{ tt },
      lexeme{ l }
    {}

    int getTokenType() const { return tokenType; }


  private:
    int tokenType;
    std::string lexeme;
};

#endif
