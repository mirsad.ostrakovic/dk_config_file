#ifndef PARSER_H
#define PARSER_H

#include <vector>
#include <iostream>
#include "token.h"

//  CFG productions rules:
//  --------------------------------------
//    KONF -> RED '\n' KONF | epsilon
//    RED  -> id '=' VRIJEDNOST | sekcija
//    VRIJEDNOST -> num | string
//  --------------------------------------

class Parser{
  public:
    Parser(const std::vector<Token>& tokens): tokensList{ tokens } {}
    bool parse();

  private:
    std::vector<Token> tokensList;
    std::size_t pos;
   
    bool match(int tokenType);
    bool KONF();
    bool RED();
    bool VRIJEDNOST();
    bool epsilon();


};

#endif
