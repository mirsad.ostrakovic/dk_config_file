// Potrebo je provjeriti sintaksiku korektnost konfiguracionog fajla.
// Konfiguracioni fajl se sastoji od proizvoljnog broja sekcija, a svaka sekcija sadrzi proizvoljan broj redova.
// Svaka sekcija pocinje oznakom #, a zatim slijedi tekst koji je ujedno i naziv sekcije(npr. #Globalni_parametri).
// U svakom redu je upisana vrijednost jednog konfiguracionog parametra u formi parametar = vrijednost.
// Nadalje, parametar odgovara pravilima imenovanja varijable u C, a njegova vrijednost moze biti ili cijeli broj
//  ili tekst unutar dvostrukih navodnika. 
//
//  Gramatika(CFE) za datu sintaksu konfiguracionog fajla je sljedeca:
//    KONF -> RED '\n' KONF | epsilon
//    RED  -> id '=' VRIJEDNOST | sekcija
//    VRIJEDNOST -> num | string
//
// Potrebno je napisati RDP parser u c++ programskom jeziku. Pri tome je potrebno odgovoriti na dole navede tacke.
//  
//  - Da li je potrebno izvrsiti eliminaciju lijeve rekurzije, kao i lijeko faktorisanje? Ako je potrebno, obrazloziti i 
//    uciniti to, a ako nije obrazloziti zasto nije!
//
//  - U nastavku je dat sadrzaj fajla 'main.cpp'. Potrebno je napisati minimalan sadrzaj ostalih fajlova da bi se program
//    ispravno kompajlirao sljedecim komandama:
//     $ flex konf_lekser.l
//     $ g++ -std=c++11 main.cpp lex.yy.c parser.cpp token.cpp -o konf_parser
//    Objasniti gore navedene komande, kao i sta predstavljaju pojedini fajlovi!
//    Provjeriti ispravnost konfiguracije u fajlu konfiguracija.conf vrsi se sljedecom komandom
//     $ konf_parser < konfiguracija.conf
